import  {  combineReducers } from 'redux';


import filterReducer from './filters'
import cartReducer from './cart'
import pizzasReducer from './pizzas'

const rootReducers = combineReducers({
    filter: filterReducer,
    pizzas:pizzasReducer,
    cart: cartReducer
})

export default rootReducers 