import React, { useCallback,useEffect } from 'react'
import { Categories, SortPopup, PizzaBlock } from '../Components'
import LoadingPizza from '../Components/LoadingPizza'
import { useSelector, useDispatch } from 'react-redux'
import  { setCategory, setSortBy }  from '../redux/actions/filters'
import  { addPizzaToCart }  from '../redux/actions/cart'
import { fetchPizzas  } from '../redux/actions/pizzas'


const categoriesItem = ['Мясные', 'Вегетарианская', 'Гриль', 'Острые', 'Закрытые']
const sortItems = [
  { name: 'популярности', type: 'popular', order: 'desc' },
  { name: 'цене', type: 'price', order: 'desc' },
  { name: 'алфавит', type: 'name', order: 'asc' },
];

  


function Home() {

  const items = useSelector(({ pizzas }) => pizzas.items)
  const cartItems = useSelector(({ cart }) => cart.items)
  const isLoaded = useSelector(({ pizzas }) => pizzas.isLoaded)
  const { category, sortBy } = useSelector(({ filter }) => filter);

  const dispatch = useDispatch()
  useEffect(()=>{
      dispatch(fetchPizzas(sortBy, category)) 
  },[category, sortBy])

  
  const onSelectItem = useCallback((index) => {
    dispatch(setCategory(index))
   
 }, [])  

 const onSelectSortType = useCallback((type) => {
    dispatch(setSortBy(type))
 }, [])

 const handdleAddPizzaToCart = obj => {
   dispatch(addPizzaToCart(obj))
   console.log(cartItems)
 }
 return (
        <div className="container">
        <div className="content__top">
          <Categories  onClickItem={onSelectItem} activeCategory={category} categoriesItem={categoriesItem}/>
          <SortPopup activeSortType={sortBy } items={sortItems} onClicSortType={onSelectSortType}/>
        </div>
        <h2 className="content__title">Все пиццы</h2>
        <div className="content__items">
      { isLoaded  ? items.map((obj,index) => <PizzaBlock onClickAddPizza={ handdleAddPizzaToCart } key={`${index}_pb`} addedCount={cartItems[obj.id] && cartItems[obj.id].items.length} {...obj}/>)
       : Array(4).fill(0).map((_,index) => {
        return <LoadingPizza key={`${index}_lp`} />
       })}
       
        </div>
      </div>
    )
}

export default Home
