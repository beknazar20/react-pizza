import React from 'react';
import ContentLoader from "react-content-loader"

const LoadingPizza = () => {
  return(
    <ContentLoader 
    speed={0}
    width={280}
    height={460}
    viewBox="0 0 280 460"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
    
  >
    <circle cx="134" cy="118" r="118" /> 
    <rect x="0" y="254" rx="6" ry="0" width="312" height="33" /> 
    <rect x="0" y="299" rx="6" ry="0" width="273" height="81" /> 
    <rect x="0" y="410" rx="6" ry="0" width="84" height="36" /> 
    <rect x="116" y="409" rx="25" ry="25" width="161" height="36" />
  </ContentLoader>
  )
}

export default LoadingPizza