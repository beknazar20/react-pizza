import React, { useState, memo } from 'react'

const  Categories = memo(({activeCategory, categoriesItem, onClickItem})=> {

   return (
       <div>
           <div className="categories">
           <ul>
             <li  className = {activeCategory === null ? 'active' :  ''} onClick = { () => onClickItem(null)}>Все</li>
             
           {categoriesItem.map((name,index) => {
              return <li  className = {activeCategory === index ? 'active' :  ''} onClick = { () => onClickItem(index)} key={`${name}_${index}`}>{name}</li>
           })}
           </ul>
         </div>
       </div>
   )
}
)

// class  Categories extends Component {
//     state = {
//         activeItem: 3
//     }

//     onSelectItem = (index) => {
//         this.setState({
//             activeItem: index
//         })
//     }
//     render(){
//         const {categoriesItem } = this.props
//         return (
//                     <div>
//                         <div className="categories">
//                         <ul>
//                           <li className="active">Все</li>
                          
//                         {categoriesItem.map((name,index) => {
//                            return <li 
//                            className = {this.state.activeItem === index ? 'active' :  ''}
//                            onClick = { () => this.onSelectItem(index)} key={`${name}_${index}`}>{name}</li>
//                         })}
//                         </ul>
//                       </div>
//                     </div>
//                 )
//             } 
//     }



export default Categories
