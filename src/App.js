import React from 'react'
import { Header } from './Components'
import './scss/app.scss';
import { Home, Cart } from './pages'
import { Route } from 'react-router-dom'


const App = () => {

  return ( 
    <div class="wrapper">
        <Header />
    <div className="content">
        <Route path="/" exact component={Home }/>
        <Route  path="/cart" component={Cart}/> 
    </div>
  </div>
  );
}


export default App
// class App extends Component {
//   componentDidMount(){
//     fetch('http://localhost:3000/pizzas')
//       .then(resp => resp.json())
//       .then(json =>  {
//         console.log(json)
//         this.props.savePizzas(json)
//       }) 
//     }
  
//   render(){
//     console.log(this.props.items)
//     return (
//       <div class="wrapper">
//           <Header />
//       <div className="content">
//           <Route exact render={() => <Home pizzas={this.props.items}/>}/>
//           < Route  path="/cart" component={Cart}/> 
//       </div>
//     </div>
//     );
//   }
// }

// const mapStateToProps = state => {
  
//   return {
//     items: state.pizzas.items ,
//     filters: state.filters
//   }
// }

// const mapDispatchToProps = (dispatch) => {
//     return {
//     savePizzas: (items) => dispatch( setPizzasAction(items))
//   }
// }
// export default connect(mapStateToProps,mapDispatchToProps)(App);
